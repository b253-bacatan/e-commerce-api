const express = require("express");
const router = express.Router();

const productController = require("../controllers/productController");
const auth = require("../auth");


// ======================================== Start of Add New Product ==========================================


router.post("/addProduct", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
	productController.addProduct(req.body).then(resultFromController => res.send(resultFromController)).catch(err => res.send(err));

	} else {

		return false;
	}

});


// ======================================== End of Add New Product ================================================





// =================================== Start of Retrieve Active product =================================


router.get("/", (req, res) => {

	productController.getAllActive().then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err));

});

// =================================== End of Retrieve Active product =================================





// ==================================== Start of Retrieve All Product =================================

router.get("/all", auth.verify, (req, res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){
		productController.getAllProduct().then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	} else {

		res.send(false);
	}
});

// ==================================== End of Retrieve All Product =================================================





// =================================== Start of Retrieve a specific product ==========================================

router.get("/:productId", (req, res) => {

	productController.getProduct(req.params)
	.then(resultFromController => res.send(resultFromController))
	.catch(err => res.send(err))
});

// =================================== End of Retrieve a specific product ==========================================




//==================================== Start of Updating a product ====================================================


router.put("/:productId", auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		productController.updateProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	
	} else {

		res.send(false);
	}
});


//==================================== End of Updating a product ====================================================


// ================================== Start of Archiving a product ===================================================

router.put("/:productId/archive", auth.verify, (req,res) => {

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin){

		productController.archiveProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	} else {

		res.send(false)
	}
});

// ================================== End of Archiving a product ===================================================




// ================================== Start of Activating a product =================================================


router.put("/:productId/activate", auth.verify, (req, res) =>{

	const userData = auth.decode(req.headers.authorization);

	if(userData.isAdmin) {

		productController.activateProduct(req.params, req.body)
		.then(resultFromController => res.send(resultFromController))
		.catch(err => res.send(err));
	} else {

		res.send(false)
	}
});


// ================================== End of Activating a product =================================================

module.exports = router;

