const Product = require("../models/Product");

const auth = require("../auth");



// ====================================== Start of adding new product ====================================================

module.exports.addProduct = (reqBody) => {

	let newProduct = new Product({

		name : reqBody.name,
		description : reqBody.description,
		price : reqBody.price
	
	});

	return newProduct.save().then(product => true)
	.catch(err => false);
};

// ====================================== End of adding new product ====================================================


// =================================== Start of Retrieve Active product ================================================


module.exports.getAllActive = () => {

	return Product.find({ isActive : true })
	.then(result => result)
	.catch(err => err);
};


// =================================== End of Retrieve Active product ===================================================

module.exports.getAllProduct = () => {

	return Product.find({}).then(result => result)
	.catch(err => err);
};


// =================================== Start of Retrieve a specific product ==========================================


module.exports.getProduct = reqParams => {

	return Product.findById(reqParams.productId)
	.then(result => {

		return result;

	}).catch(err => err);
}


// =================================== End of Retrieve a specific product =============================================



//==================================== Start of Updating a product ====================================================



module.exports.updateProduct = (reqParams, reqBody) => {

	let updatedProduct = {

		name : reqBody.name,
		description : reqBody.description
	
	};

	return Product.findByIdAndUpdate(reqParams.productId, updatedProduct)
	.then(product => {

		return product;
	})
	.catch(err => err);

};


//==================================== End of Updating a product =====================================================


// ================================== Start of Archiving a product ===================================================


module.exports.archiveProduct = (reqParams, reqBody) => {

	let archivedProduct = {

		isActive : false

	};

	return Product.findByIdAndUpdate(reqParams.productId, archivedProduct)
	.then(product => {

		return true;
	})
	.catch(err => err);
};

// ================================== End of Archiving a product ===================================================




// ================================== Start of Activating a product =================================================


module.exports.activateProduct = (reqParams, reqBody) => {

	let activatedProduct = {

		isActive : true
	};

	return Product.findByIdAndUpdate(reqParams.productId, activatedProduct)
	.then(product => {

		return true;

	})
	.catch(err => err);


};


// ================================== End of Activating a product =================================================




