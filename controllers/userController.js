const User = require("../models/User");

const Product = require("../models/Product")

const bcrypt = require("bcrypt");

const auth = require("../auth");


// ========================================= Start of creating new user ===================================================

module.exports.checkEmailExists = (reqBody) => {

	// The result is sent back to the frontend via the "then" method found in the route file
	return User.find({ email : reqBody.email }).then(result => {

		// The "find" method returns a record if a match is found
		if(result.length > 0) {

			return true;
		// No duplicate email found
		// The user is not yet registered in the database
		} else {

			return false;
		}
	}).catch(err => err);
};





module.exports.registerUser = (reqBody) => {

			let newUser = new User ({
				firstName : reqBody.firstName,
				lastName : reqBody.lastName,
				email : reqBody.email,
				password : bcrypt.hashSync(reqBody.password, 10),
				isAdmin : reqBody.isAdmin
	});
			return newUser.save().then(user => {

			if(user) {
				return true
		} else {
			return false
		}
	
	}).catch(err => err)

};

// ========================================= End of creating new user ===================================================



// ========================================= Start of authenticating a user ==============================================

module.exports.loginUser = (reqBody) => {

	return User.findOne({ email : reqBody.email }).then(result => {

		if(result == null) {

			return false;
		
		} else {

			const isUserCorrect = bcrypt.compareSync(reqBody.password, result.password);
		
			if(isUserCorrect) {

				return { access : auth.createAccessToken(result) }
			
			} else {

				return false;
			}

		}

	}).catch(err => err);

};


// ========================================= End of authenticating a user ===================================================



// ========================================= Start of ordering a product ==============================================

module.exports.checkout = (data, body) => {

	const id = data.userId

	return User.findById(id).then((user) => {

		user.orderedProduct.push({ products : body })

		return user.save().catch(err => err)

	}).catch(err => err)
}


// ========================================= End of ordering a product ==============================================


// ========================================= Start of retreiving details ==============================================


module.exports.getDetails = (data) => {

	return User.findById(data).then(result => {

		result.password = ""

		return result;
	})
};

// ========================================= End of retreiving details ==============================================


